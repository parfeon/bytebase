create table users
(
    id        integer      not null
        constraint users_pk
        primary key,
    name      varchar(255) not null,
    email     varchar(255),
    is_active integer default 0
);

create table comments
(
    id      integer
        constraint comments_pk
        primary key,
    user_id integer
        constraint comments_pk_2
        unique,
    text    text
);